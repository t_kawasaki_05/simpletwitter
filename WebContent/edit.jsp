<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>つぶやき編集画面</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <div class ="header">
                <a href = "./">ホーム</a>
                <a href = "setting">設定</a>
<a href = "logout">ログアウト</a>
            </div>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <div class="form-area">
                    <form action="edit" method="post">
                        <input type="hidden" name="messageId" value="${editMessage.id}">
                        つぶやき<br />
                        <textarea name="message" cols="100" rows="5" class="tweet-box"><c:out value="${editMessage.text}"/></textarea>
                        <br />
                        <input type = "submit" value="更新">
                    </form>
            </div>
            <div class="bottom">
                <a href ="./">戻る</a>
            </div>
            <div class="copyright">Copyright(c)TK</div>
        </div>
    </body>
</html>