package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
        }
    }

	public void update(String message, int messageId) {

		Connection connection =  null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback (connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Message select(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();

			Message messages = new MessageDao().select(connection,messageId);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public List<UserMessage> select(String userId, String start, String end) {
		final int LIMIT_NUM = 1000;
		String startDate = null;
		String endDate = null;
		Date defaultEndDate = new Date();
		SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );

		Connection connection = null;
		try {
			connection = getConnection();

			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}
			if (!StringUtils.isBlank(start)) {
				startDate = (start +  " " + "00:00:00");
			}else {
				startDate = "2021-12-01 00:00:00";
			}
			if (!StringUtils.isBlank(end)) {
				endDate = (end +  " " + "23:59:59");
			}else {
				endDate = format.format(defaultEndDate);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate,  endDate);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}