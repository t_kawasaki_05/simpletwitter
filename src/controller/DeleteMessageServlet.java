package controller;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })

public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int messageId = Integer.parseInt(request.getParameter("id"));

		new MessageService().delete(messageId);

		response.sendRedirect("./");
	}
}
