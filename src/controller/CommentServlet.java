package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = {"/comment"})

public class CommentServlet extends HttpServlet {

protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

    HttpSession session = request.getSession();
    List<String> errorMessages = new ArrayList<String>();
    Comment comment = new Comment();

    String text = request.getParameter("text");
    int messageId = Integer.parseInt(request.getParameter("messageId"));
    if (!isValid(text, errorMessages)) {
    	comment.setText(text);
    	session.setAttribute("comment", comment);
        session.setAttribute("errorMessages", errorMessages);
        response.sendRedirect("./");
        return;
    }
	User user = (User) session.getAttribute("loginUser");

    comment.setUserId(user.getId());
    comment.setText(text);
    comment.setMessageId(messageId);

    new CommentService().insert(comment);
    response.sendRedirect("./");
    }
    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
