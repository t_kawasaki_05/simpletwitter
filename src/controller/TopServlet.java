package controller;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp"})

public class TopServlet extends HttpServlet {

	private static final long serialVersionUID = 1;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String startDate = null;
		String endDate = null;
		HttpSession session = request.getSession();

		boolean isShowMessageForm = false;

		User user = (User) request.getSession().getAttribute("loginUser");
		String userId = request.getParameter("user_id");
		startDate = request.getParameter("startDate");
		endDate = request.getParameter("endDate");

		if (user != null) {
			isShowMessageForm = true;
		}
		List<UserMessage> messages = new MessageService().select(userId, startDate, endDate);
		List<UserComment> comments = new CommentService().select();
		session.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}