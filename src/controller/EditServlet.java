package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import service.MessageService;

@WebServlet(urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<String> errorMessages = new ArrayList<String>();

		String id = request.getParameter("id");

		Message message = null;
		if(!StringUtils.isBlank(id) && id.matches("^[0-9]*$")) {
			int messageId = Integer.parseInt(id);
			message = new MessageService().select(messageId);
		}
		if(message == null){
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./").forward(request, response);
			return;
		}
		session.setAttribute("editMessage", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String editMessage = request.getParameter("message");
		int messageId = Integer.parseInt(request.getParameter("messageId"));

		if (!isValid(editMessage, errorMessages)) {
			Message message = new MessageService().select(messageId);
			message.setText(editMessage);
			session.setAttribute("editMessage", message);
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("edit.jsp");
			return;
		}
		new MessageService().update(editMessage, messageId);

		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}